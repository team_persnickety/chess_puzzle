# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Spring Boot REST application that exposes endpoints to ChessPuzzles
  - Created starter project with: https://start.spring.io/
* 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* Project located in /home/tim/Desktop/chess_spring_rest/chess_puzzle

### How do I get set up? ###

* Summary of set up
* Configuration
  - Run configurations created in IDE are usually not saved to git. So changes to them can be lost. 
  - To run in IntelliJ while on server running Tomcat, it is needed to change port from 8080 (or whatever port Tomcat is running on). 
    - Add to the VM options of Run configuration: 
      - -Dserver.port=9080 
* Dependencies
* Database configuration
  - PostgreSQL
    - Linux command-line tool *pg_isready* - tells if postgres is accepting connections
    - pg_isready -d upcdb -h localhost -p 5432 -U postgres
    - Enter interactive database tool: sudo -u postgres psql 
        - https://www.postgresqltutorial.com/psql-commands/
    - Data Types: https://www.postgresql.org/docs/9.5/datatype.html

* Deployment instructions
  - To Tomcat: mvn -Dmaven.test.skip=true clean install
    - Creates a WAR which can be loaded into Tomcat server

* Kotlin scripts for creating SQL statements
    - Script files have a .kt extension. A Kotlin plugin was installed in VS Code.
    - compile: kotlinc-jvm Hello.kt -include-runtime -d Hello.jar
    - run jar: kotlin -classpath ./ Hello.jar Hello
    - Example SQL file to insert puzzles is insert_puzzles.sql

### Contribution guidelines ###
* WIP

### Who do I talk to? ###

* Tim@persnicketysoft.com
* William@persnicketysoft.com
