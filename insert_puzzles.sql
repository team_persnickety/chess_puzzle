
INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) 
values('0000D', '5rk1/1p3ppp/pq3b2/8/8/1P1Q1N2/P4PPP/3R2K1 w - - 2 27', 'd3d6 f8d8 d6d8 f6d8', 1426, 500,2,0,'advantage endgame short','https://lichess.org/F8M8OS71#53');

INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) 
values('0009B','r2qr1k1/b1p2ppp/pp4n1/P1P1p3/4P1n1/B2P2Pb/3NBP1P/RN1QR1K1 b - - 1 16','b6c5 e2g4 h3g4 d1g4',1500,500,2,0,'advantage middlegame short','https://lichess.org/4MWQCxQ6/black#32');

INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) 
values('000tp','4r3/5pk1/1p3np1/3p3p/2qQ4/P4N1P/1P3RP1/7K w - - 6 34','d4b6 f6e4 h1g1 e4f2',1687,500,-5,0,'crushing endgame short trappedPiece','https://lichess.org/GeXqsW90#67');

INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) 
values('001cv','6k1/5p1p/3p2pP/3Pr1b1/1pp5/7R/PP6/1K1R4 w - - 0 34','d1d4 e5e1 d4d1 e1d1 b1c2 d1c1',1043,500,-15,0,'endgame long mate mateIn3 queensideAttack','https://lichess.org/Wn5Xtz5X#67');

INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) 
values('001u3','2r3k1/p1q2pp1/Q3p2p/b1Np4/2nP1P2/4P1P1/5K1P/2B1N3 b - - 3 33','c7b6 a6c8 g8h7 c8b7',1268,500,2,0,'advantage hangingPiece kingsideAttack middlegame short','https://lichess.org/BBn6ipaK/black#66');

INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) 
values('00206','r3kb1r/pppqpn1p/5p2/3p1bpQ/2PP4/4P1B1/PP3PPP/RN2KB1R w KQkq - 1 11','b1c3 f5g4 h5g4 d7g4',1236,500,-5,0,'advantage opening short trappedPiece','https://lichess.org/MbJRo6PT#21');

INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) 
values('002Cw','r7/2p2r1k/p2p1q1p/Pp1P4/1P2P3/2PQ4/6R1/R5K1 b - - 2 28','f7g7 e4e5 f6g6 g2g6',1315,500,-5,0,'crushing discoveredAttack endgame short','https://lichess.org/lxiSa85s/black#56');

INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) 
values('002E4','8/8/kpq5/p4pQp/P7/7P/3r2P1/4R2K b - - 10 48','c6a4 g5d2',1406,500,-5,0,'crushing endgame hangingPiece oneMove','https://lichess.org/JwMca3Nw/black#96');

INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) 
values('002rj','2k5/p1pp1p2/1p3Bb1/7p/1nP3PP/1P2rP2/PK6/3R1B1R w - - 7 22','f1g2 e3e2 d1d2 e2d2',960,500,-5,0,'advantage fork middlegame short','https://lichess.org/JbCsF5hm#43');

INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) 
values('003YT','r1bqk1nr/1pp2ppp/p1pb4/4p3/3PP3/5N2/PPP2PPP/RNBQ1RK1 b kq - 0 6','d8f6 d4e5 d6e5 c1g5 f6d6 f3e5 d6d1 f1d1',1387,500,-5,0,'advantage fork opening veryLong','https://lichess.org/TAffogpQ/black#12');
