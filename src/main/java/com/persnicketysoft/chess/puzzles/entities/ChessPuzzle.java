package com.persnicketysoft.chess.puzzles.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "chess_puzzle")
public class ChessPuzzle {

    @Id
    private String id;
    private String gameurl;
    private Integer nbplays;
    private Integer popularity;
    private Integer ratingdeviation;
    @Column(nullable = false)
    private Integer rating;
    private String moves;
    private String fen;
    @Column(nullable = false, name = "Themes")
    private Long themes;

    public ChessPuzzle(String id, String gameurl, Integer nbplays, Integer popularity, Integer ratingdeviation, Integer rating, String moves, String fen, Long themes) {
        this.id = id;
        this.gameurl = gameurl;
        this.nbplays = nbplays;
        this.popularity = popularity;
        this.ratingdeviation = ratingdeviation;
        this.rating = rating;
        this.moves = moves;
        this.fen = fen;
        this.themes = themes;
    }

    //required no-arg constructor
    public ChessPuzzle() {

    }

    public String getId() {
        return id;
    }

    public String getGameurl() {
        return gameurl;
    }

    public Integer getNbplays() {
        return nbplays;
    }

    public Integer getPopularity() {
        return popularity;
    }

    public Integer getRatingdeviation() {
        return ratingdeviation;
    }

    public Integer getRating() {
        return rating;
    }

    public String getMoves() {
        return moves;
    }

    public String getFen() {
        return fen;
    }

    public Long getThemes() {
        return themes;
    }

    public String getResourceId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setGameurl(String gameurl) {
        this.gameurl = gameurl;
    }

    public void setNbplays(Integer nbplays) {
        this.nbplays = nbplays;
    }

    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

    public void setRatingdeviation(Integer ratingdeviation) {
        this.ratingdeviation = ratingdeviation;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public void setMoves(String moves) {
        this.moves = moves;
    }

    public void setFen(String fen) {
        this.fen = fen;
    }

    public void setThemes(Long themes) {
        this.themes = themes;
    }
}
