package com.persnicketysoft.chess.puzzles.repos;

import com.persnicketysoft.chess.puzzles.entities.ChessPuzzle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


//See https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation
public interface ChessRepository extends CrudRepository<ChessPuzzle, String> {
    Page<ChessPuzzle> findByThemes(@Param("themes") Long theme, Pageable pageable);

    @Query("select cp from ChessPuzzle cp where cp.themes = ?1 and cp.rating >= ?2")
    Page<ChessPuzzle> findByThemesAndRating(@Param("themes") Long theme, @Param("rating") Integer rating, Pageable pageable);
}
