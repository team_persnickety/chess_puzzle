CREATE TABLE chess_puzzle (

   id VARCHAR (20) NOT NULL PRIMARY KEY,
   gameUrl VARCHAR (50),
   nbPlays INT,
   popularity INT,
   ratingDeviation INT,
   rating INT,
   moves VARCHAR (200),
   fen VARCHAR (100),
   themes BIGINT
);